#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --output-file=requirements/py3.9/tests.txt requirements/tests.in
#
aiofiles==0.4.0
    # via aiologger
aiologger[aiofiles]==0.6.1
    # via pop-config
asynctest==0.13.0
    # via pytest-pop
attrs==21.2.0
    # via pytest
dict-toolbox==2
    # via
    #   pop
    #   pop-config
iniconfig==1.1.1
    # via pytest
mock==4.0.3
    # via
    #   -r requirements/tests.in
    #   pytest-pop
msgpack==1.0.2
    # via dict-toolbox
nest-asyncio==1.5.1
    # via
    #   pop-loop
    #   pytest-pop
packaging==20.9
    # via pytest
pidfile==0.1.1
    # via -r requirements/tests.in
pluggy==0.13.1
    # via pytest
pop-config==6.11
    # via pop
pop-loop==1.0.3
    # via pop
pop==20.0.0
    # via
    #   pop-config
    #   pop-loop
    #   pytest-pop
proxy-tools==0.1.0
    # via pop
psutil==5.8.0
    # via -r requirements/tests.in
py==1.10.0
    # via pytest
pyparsing==2.4.7
    # via packaging
pytest-asyncio==0.15.1
    # via
    #   -r requirements/tests.in
    #   pytest-pop
pytest-httpserver==1.0.1
    # via -r requirements/tests.in
pytest-pop==8.0.0
    # via -r requirements/tests.in
pytest-tempdir==2019.10.12
    # via -r requirements/tests.in
pytest==6.2.4
    # via
    #   -r requirements/tests.in
    #   pytest-asyncio
    #   pytest-pop
    #   pytest-tempdir
pyyaml==5.4.1
    # via
    #   dict-toolbox
    #   pop
sniffio==1.2.0
    # via pop-loop
toml==0.10.2
    # via pytest
werkzeug==2.0.1
    # via pytest-httpserver
