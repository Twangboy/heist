.. _configuration-heist:

=================
Configuring Heist
=================

The configuration file for Heist is located at ``/etc/heist/heist.conf`` on
Linux and ``C:\ProgramData\heist\heist.conf`` on windows
by default. The ``C:\ProgramData`` portion of the path might be different in
your environment if your ``ProgramData`` environment variable is set to a
different path. If the ``ProgramData`` environment variable is not set on
your Windows OS, then Heist will use ``C:\ProgramData`` by default.


You can change the location of the heist configuration file two ways:

cli
---

You can pass `-c /opt/heist.conf` on the cli when running Heist. This
example would use the file /opt/heist.conf for the Heist configuration file.

environment variable
--------------------

You can set the environment variables `HEIST_CONFIG` to the path of the
configuration you want to use for Heist.


.. _primary-heist-configuration:

Primary Heist Configuration
===========================

.. conf_heist:: noclean

``noclean``
-------------

Default: ``False``

Do not clean up the minion when the Heist connection is stopped.

.. code-block:: yaml

    noclean: True
