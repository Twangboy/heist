All notable changes to Heist will be documented in this file.

This changelog follows [keepachangelog](https://keepachangelog.com/en/1.0.0/) format, and is intended for human consumption.

# Changelog

Heist v6.1.0 (2021-12-07)
=========================

Added
-----

- Add recurse argument for asyncssh.send to allow user to copy recursively a directory. (#123)


Heist v6.0.1 (2021-11-04)
=========================

Fixed
-----

- Fix cleanup when targeting multiple hosts. (#121)


Heist v6.0.0 (2021-10-25)
=========================

Removed
-------

- The windows default configuration paths have been moved from `C:\heist\` to `C:\ProgramData\heist\`. (#100)


Deprecated
----------

- The default Windows configuration paths will change from C:\heist to C:\ProgramData\heist in Heist version v6.0.0. (#113)


Fixed
-----

- Allow heist to use environment variable to set config file. (#117)
- Fix traceback when using config artifacts_dir. (#118)
- Enforce the flat roster to be the default. (#119)


Added
-----

- Add towncrier tool to the heist project to help manage CHANGELOG.md file. (#110)
- Add a test Heist manager for users to test Heist installation and setup. (#114)
- Migrate artifact calls to heist/artifact/init.py (#115)
