async def disable(hub, tunnel_plugin, target_name, service):
    pass


async def enable(hub, tunnel_plugin, target_name, service):
    pass


async def start(hub, tunnel_plugin, target_name, service, run_cmd=None, **kwargs):
    """
    Start the service in the background
    """
    cmd = [f"{run_cmd}"]
    ret = await hub.tunnel[tunnel_plugin].cmd(
        target_name, " ".join(cmd), background=True
    )
    if ret.returncode != 0:
        hub.log.error(ret.stderr)
        return False
    return True


async def stop(hub, tunnel_plugin, target_name, service):
    """
    Stop the service
    """
    ret = await hub.tunnel[tunnel_plugin].cmd(target_name, f"pkill -f {service}")
    if ret.exit_status != 0:
        hub.log.error(ret.stderr)
        return False
    return True


async def restart(hub, tunnel_plugin, target_name, service):
    pass


def conf_path(hub, service_name):
    pass


async def clean(hub, target_name, tunnel_plugin):
    pass
