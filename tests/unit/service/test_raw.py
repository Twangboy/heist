from unittest.mock import call

import pytest


@pytest.mark.asyncio
async def test_start_service(hub, mock_hub):
    """
    Test starting a raw service
    """
    target_name = "1234"
    mock_hub.service.raw.start = hub.service.raw.start
    await mock_hub.service.raw.start(
        "asyncssh", target_name, "test-service", "/path/to/run -c /path/to/config"
    )
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, "/path/to/run -c /path/to/config", background=True
    )


@pytest.mark.asyncio
async def test_stop_service(hub, mock_hub):
    """
    Test stopping a raw service
    """
    target_name = "1234"
    service_name = "test-service"
    mock_hub.service.raw.stop = hub.service.raw.stop
    await mock_hub.service.raw.stop("asyncssh", target_name, service_name)
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"pkill -f {service_name}"
    )
