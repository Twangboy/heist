#!/usr/bin/python3
"""
    tests.unit.heist.test_heist
    ~~~~~~~~~~~~~~

    tests for heist.init code
"""
import pathlib
import sys
import unittest.mock as mock
from unittest.mock import call
from unittest.mock import Mock
from unittest.mock import patch

import pytest
from dict_tools.data import NamespaceDict

import heist.heist.init
import tests.helpers


@pytest.mark.skipif(sys.platform == "win32", reason="Does not run on Windows.")
def test_start_nix(hub, mock_hub):
    mock_hub.heist.init.start = hub.heist.init.start_nix
    mock_hub.heist.init.start()
    mock_hub.pop.loop.start.assert_called_once()
    mock_hub.heist.init.run_remotes.assert_called_once()


@pytest.mark.skipif(sys.platform == "win32", reason="Does not run on Windows.")
def test_start_no_manager_nix(hub, mock_hub):
    """
    Test the start method without defining a manager
    """
    mock_hub.heist.init.start = hub.heist.init.start_nix
    mock_hub.SUBPARSER = ""
    assert not mock_hub.heist.init.start()


@pytest.mark.asyncio
async def test_run_remotes(mock_hub, hub, tempdir):
    mock_hub.heist.init.run_remotes = hub.heist.init.run_remotes
    mock_hub.OPT.heist = {"roster": mock.sentinel.roster, "manager": "test"}

    await mock_hub.heist.init.run_remotes("test", tempdir)

    mock_hub.roster.init.read.assert_called_once_with(
        None, roster_file="", roster_data=None
    )


@pytest.mark.asyncio
async def test_run_roster_false(mock_hub, hub, tempdir):
    """
    test heist.heist.init when roster does not render correctly
    """
    mock_hub.heist.init.run_remotes = hub.heist.init.run_remotes
    mock_hub.OPT.heist = {"roster": mock.sentinel.roster, "manager": "salt_master"}
    mock_hub.roster.init.read.return_value = {}

    await mock_hub.heist.init.run_remotes("test", tempdir)

    mock_hub.roster.init.read.assert_called_with(None, roster_file="", roster_data=None)


@pytest.mark.parametrize(
    "addr",
    [
        ("127.0.0.1", True),
        ("::1", True),
        ("2001:0db8:85a3:0000:0000:8a2e:0370:7334", False),
        ("localhost", True),
        ("1.1.1.1", False),
        ("google.com", False),
    ],
)
def test_ip_is_loopback(addr, mock_hub):
    """
    Test for function ip_is_loopback
    when socket error raised, expected
    return is False
    """
    ret = heist.heist.init.ip_is_loopback(mock_hub, addr[0])
    assert ret == addr[1]


def test_ip_is_loopback_exception(mock_hub):
    """
    Test for function ip_is_loopback
    when address is not valid
    """
    assert not heist.heist.init.ip_is_loopback(mock_hub, "")


@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.asyncio
async def test_clean(mock_hub, hub, tempdir):
    """
    test cleanup
    """
    con1 = "123456789"
    con2 = "987654321"
    mock_hub.heist.CONS = {
        con1: {"manager": "salt.minion", "tunnel_plugin": "asyncssh"},
        con2: {"manager": "salt.minion", "tunnel_plugin": "asyncssh"},
    }
    mock_hub.heist.ROSTERS = {
        con1: {"host": "192.168.1.1"},
        con2: {"host": "192.168.1.2"},
    }
    mock_hub.OPT = NamespaceDict(heist={"noclean": False})
    mock_hub.heist.init.clean = hub.heist.init.clean
    tests.helpers.mock_manager(mock_hub)
    await mock_hub.heist.init.clean()
    assert mock_hub.heist.salt.minion.clean.call_args_list == [
        call(
            con1,
            "asyncssh",
            service_plugin=None,
            vals={"manager": "salt.minion", "tunnel_plugin": "asyncssh"},
        ),
        call(
            con2,
            "asyncssh",
            service_plugin=None,
            vals={"manager": "salt.minion", "tunnel_plugin": "asyncssh"},
        ),
    ]


@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.asyncio
async def test_clean_noclean_set(mock_hub, hub, tempdir):
    """
    test cleanup when noclean is set
    """
    mock_hub.heist.CONS = {
        "testconnection": {"manager": "salt.minion", "tunnel_plugin": "asyncssh"}
    }
    mock_hub.heist.ROSTERS = {"test_host": {"manager": "salt.minion"}}
    mock_hub.OPT = NamespaceDict(heist={"noclean": True})
    mock_hub.heist.init.clean = hub.heist.init.clean
    tests.helpers.mock_manager(mock_hub)
    await mock_hub.heist.init.clean()
    mock_hub.heist.salt.minion.clean.assert_not_called()


def test_config_paths(mock_hub, hub, tmp_path):
    """
    test paths in config
    """
    mock_hub.pop.config.load = hub.pop.config.load
    heist_conf = tmp_path / "heist.conf"
    heist_conf.write_text("")
    with mock.patch("sys.argv", ["heist", "test", f"-c={heist_conf}"]):
        hub.pop.config.load(["heist"], cli="heist")
        if sys.platform == "linux":
            linux_root = pathlib.Path("/etc", "heist")
            assert hub.OPT.heist.config == str(heist_conf)
            assert hub.OPT.heist.roster_dir == str(linux_root / "rosters")
            assert hub.OPT.heist.artifacts_dir == str(
                pathlib.Path("/var", "tmp", "heist", "artifacts")
            )
        elif sys.platform == "win32":
            windows_root = pathlib.Path("C:\\ProgramData", "heist")
            assert hub.OPT.heist.config == str(heist_conf)
            assert hub.OPT.heist.roster_dir == str(windows_root / "rosters")
            assert hub.OPT.heist.artifacts_dir == str(windows_root / "artifacts")
        else:
            raise AssertionError("Did not find OS. Need to add tests for designated OS")
