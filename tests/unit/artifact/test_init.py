#!/usr/bin/python3
"""
    tests.unit.artifact.test_init
    ~~~~~~~~~~~~~~

    tests for artifact.init code
"""
import pathlib
import sys
import tarfile
import tempfile
import unittest.mock as mock
import zipfile
from unittest.mock import call
from unittest.mock import MagicMock
from unittest.mock import Mock
from unittest.mock import patch

import aiohttp
import pytest
from dict_tools.data import NamespaceDict

import tests.helpers
from tests.helpers import TEST_FILES


@pytest.mark.asyncio
async def test_clean(mock_hub, hub):
    """
    test artifact.init.clean removes the correct directory
    """
    mock_hub.artifact.init.clean = hub.artifact.init.clean
    target_name = "test_target_name"
    run_dir = pathlib.Path("var") / "tmp" / "heist_root" / "abcd"
    mock_hub.heist.CONS = {}
    mock_hub.heist.CONS[target_name] = {}
    mock_hub.heist.CONS[target_name]["run_dir"] = run_dir

    await mock_hub.artifact.init.clean(target_name, "asyncssh")

    mock_hub.tunnel.asyncssh.cmd.call_args_list[0] == call(
        target_name, f"rm -rf {run_dir}"
    )
    mock_hub.tunnel.asyncssh.cmd.call_args_list[1] == call(
        target_name, f"rmdir {run_dir.parent}"
    )


@pytest.mark.parametrize(
    "hash_value,expected",
    [
        (
            "a69f73cca23a9ac5c8b567dc185a756e97c982164fe25859e0d1dcc1475c80a615b2123af1f5f94c11e3e9402c3ac558f500199d95b6d3e301758586281dcd26",
            True,
        ),
        ("1234", False),
    ],
)
def test_verify(hub, mock_hub, hash_value, expected):
    """
    test verify when hash is correct
    """
    mock_hub.artifact.init.verify = hub.artifact.init.verify
    if expected:
        assert mock_hub.artifact.init.verify(
            location=TEST_FILES / "sha_file_test",
            hash_value=hash_value,
            hash_type="sha3_512",
        )
    else:
        assert not mock_hub.artifact.init.verify(
            location=TEST_FILES / "sha_file_test",
            hash_value=hash_value,
            hash_type="sha3_512",
        )


@pytest.mark.asyncio
async def test_fetch(hub, mock_hub, httpserver):
    """
    test artifacts.init.fetch when 200 returned
    and download or location are not set
    """
    mock_hub.artifact.init.fetch = hub.artifact.init.fetch
    exp_ret = {"test": "true"}
    httpserver.expect_request("/test").respond_with_json(exp_ret)
    url = httpserver.url_for("/test")
    async with aiohttp.ClientSession() as session:
        ret = await mock_hub.artifact.init.fetch(session=session, url=url)
    assert ret == exp_ret


def test_extract(hub, mock_hub):
    """
    test artifact.init.extract
    """
    files_in_archive = ["test", "test2"]
    mock_hub.artifact.init.extract = hub.artifact.init.extract

    def _clean_extracted_files(tmpdirname):
        for file_name in files_in_archive:
            full_path = pathlib.Path(tmpdirname) / file_name
            assert full_path.exists()
            full_path.unlink()
            assert not full_path.exists()

    with tempfile.TemporaryDirectory() as tmpdirname:
        root_name = "test_artifact"
        tar_file = pathlib.Path(tmpdirname, f"{root_name}.tar.gz")
        zip_file = pathlib.Path(tmpdirname, f"{root_name}.zip")
        create_tar = tarfile.open(tar_file, "w:gz")
        create_zip = zipfile.ZipFile(zip_file, mode="w")
        for file_name in files_in_archive:
            full_path = pathlib.Path(tmpdirname) / file_name
            with open(full_path, "w") as fp:
                fp.write("")

            create_tar.add(full_path, arcname=file_name)
            create_zip.write(full_path, arcname=file_name)

        create_tar.close()
        create_zip.close()
        _clean_extracted_files(tmpdirname)
        assert mock_hub.artifact.init.extract(tmpdir=tmpdirname, binary=tar_file)
        _clean_extracted_files(tmpdirname)
        assert mock_hub.artifact.init.extract(tmpdir=tmpdirname, binary=zip_file)
        for file_name in files_in_archive:
            test_path = pathlib.Path(tmpdirname) / file_name
            assert test_path.exists()


@pytest.mark.parametrize(
    "version",
    [
        ("1.2.3.4-1"),
        ("1.2.3.4"),
    ],
)
@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.asyncio
async def test_get(hub, mock_hub, tmp_path, version):
    """
    test artifacts.init.get
    """
    tests.helpers.mock_artifacts(mock_hub)
    mock_hub.artifact.init.get = hub.artifact.init.get
    mock_temp_dir = tempfile.TemporaryDirectory()
    mock_hub.OPT.heist = NamespaceDict(artifacts_dir=str(tmp_path))
    files_in_archive = ["test1", "test2"]
    with tempfile.TemporaryDirectory() as tmpdirname:
        tar_file = pathlib.Path(tmpdirname, f"test_artifact-{version}.tar.gz")
        create_tar = tarfile.open(tar_file, "w:gz")
        for file_name in files_in_archive:
            full_path = pathlib.Path(tmpdirname) / file_name
            with open(full_path, "w") as fp:
                fp.write("")
            create_tar.add(full_path, arcname=file_name)
        create_tar.close()
        mock_hub.artifact.test.get.return_value = tar_file
        with patch("tempfile.TemporaryDirectory", Mock(return_value=mock_temp_dir)):
            ret = await mock_hub.artifact.init.get(
                artifact_name="test", target_os="linux", version=version, repo_data={}
            )
        exp_artifact_file = tmp_path / tar_file.name
        assert ret == exp_artifact_file
        assert exp_artifact_file.exists()


@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.asyncio
async def test_get_bad_path(hub, mock_hub, tmp_path):
    """
    test artifacts.init.get when tmp_artifact_location includes "../"
    """
    tests.helpers.mock_artifacts(mock_hub)
    mock_hub.artifact.init.get = hub.artifact.init.get
    mock_hub.tool.path.clean_path = hub.tool.path.clean_path
    mock_temp_dir = tempfile.TemporaryDirectory()
    mock_hub.OPT.heist = NamespaceDict(artifacts_dir=tmp_path)
    mock_hub.artifact.test.get.return_value = pathlib.Path("../")
    with patch("tempfile.TemporaryDirectory", Mock(return_value=mock_temp_dir)):
        ret = await mock_hub.artifact.init.get(
            artifact_name="test", target_os="linux", version="1.2.3.4", repo_data={}
        )
    assert not ret


@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.asyncio
async def test_get_tmp_art_return_false(hub, mock_hub, tmp_path):
    """
    test artifacts.init.get when tmp_artifact_location returns False
    """
    tests.helpers.mock_artifacts(mock_hub)
    mock_hub.artifact.init.get = hub.artifact.init.get
    mock_temp_dir = tempfile.TemporaryDirectory()
    mock_hub.OPT.heist = NamespaceDict(artifacts_dir=tmp_path)
    with patch("tempfile.TemporaryDirectory", Mock(return_value=mock_temp_dir)):
        mock_hub.artifact.test.get.return_value = False
        ret = await mock_hub.artifact.init.get(
            artifact_name="test", target_os="linux", version="1.2.3.4", repo_data={}
        )
    assert not ret
