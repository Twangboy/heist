import asyncio
import pathlib

import asynctest

try:
    from unittest.mock import AsyncMock
except ImportError:
    # AsyncMock was first introduced in 3.8
    AsyncMock = None

from dict_tools.data import NamespaceDict
from asyncssh.misc import async_context_manager


TESTS_DIR = pathlib.Path(__file__).resolve().parent.parent
CODE_DIR = TESTS_DIR.parent
TEST_FILES = TESTS_DIR / "files"


def async_read_write(add, port, family=None):
    reader = asynctest.mock.Mock(asyncio.StreamReader)
    writer = asynctest.mock.Mock(asyncio.StreamWriter)

    reader.read.return_value = b"SSH-2.0-OpenSSH_8.5\r\n"
    return reader, writer


# mock manager
async def mock_clean(target_name, tunnel_plugin, service_plugin=None, vals=None):
    pass


def mock_manager(mock_hub):
    mock_hub.heist.salt = NamespaceDict()
    mock_hub.heist.salt.minion = NamespaceDict()
    mock_hub.heist.salt.minion.clean = AsyncMock(mock_clean)
    return mock_hub


async def mock_get(
    target_os=None, version=None, repo_data=None, session=None, tmpdirname=None
):
    pass


def mock_artifacts(mock_hub):
    """
    mock a managers artifact
    """
    mock_hub.artifact.test = AsyncMock()
    mock_hub.artifact.test.get = AsyncMock(mock_get)
    return mock_hub


class MockAtEof:
    def at_eof(self):
        return False


class Mockstderr:
    async def readline(self):
        return ""


class Mockstdout:
    def __init__(self):
        self.eof = False
        self.read_output = "[sudo] password for ch3ll:"

    def at_eof(self):
        eof = self.eof
        self.eof = True
        return eof

    async def read(self, bytes=None):
        read_output = self.read_output
        self.read_output = "test"
        return read_output


class Mockstdin:
    def write(self, password=None):
        pass


class MockSSHClientProcess:
    def __init__(self):
        self._stdout = Mockstdout()
        self._stderr = Mockstderr()
        self._stdin = Mockstdin()

    async def __aexit__(self, _exc_type=None, _exc_value=None, _traceback=None):
        return False

    async def __aenter__(self):
        return self

    @property
    def stderr(self):
        return self._stderr

    @property
    def stdin(self):
        return self._stdin

    @property
    def stdout(self):
        return self._stdout

    @property
    def returncode(self):
        return 1


class MockAsyncsshCon:
    async def __aenter__(self):
        await log.error("entering context")

    async def __aexit__(self, exc_type, exc, tb):
        await log.error("exiting context")

    @async_context_manager
    async def create_process(self, command, term_type=None, term_size=None):
        return MockSSHClientProcess()
